struct Position
    b::Board
    # stores if white/black are still able to castle
    castling::Tuple{Bool, Bool}
    # gives last two moves (needed for en passant)
    lastmoves::Tuple{Move, Move}
end
