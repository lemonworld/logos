while true
    # read from STDIN following UCI protocol
    cmd::String = readline()
    if cmd == "uci"
        # first cmd from gui after boot
        println("id name Logos")
        println("id author Marcel Wienöbst")
        # TODO: available options (what are those)
        println("uciok")
    end
    if startswith(cmd, "debug")
        # TODO: implement debug mode and switch through 'off' and 'on'
    end
    if cmd == "isready"
        # tell GUI that I'm alive
        println("readyok")
    end
    if startswith(cmd, "setoption")
        # TODO: implement options and set those through this parameter
    end
    if startswith(cmd, "register")
        # TODO: I don't get what that means, but does not sound too important
    end
    if cmd == "ucinewgame"
        # do stuff to start game
    end
    if startswith(cmd, "position")
        # set up the position described followed by playing the given moves
    end
    if startswith(cmd, "go")
        # start to calculate on given position
        # TODO: parse all the parameters
        # if normal exit (or stop cmd) don't forget to answer with 'bestmove <move1> [ ponder <move2> ]'
    end
    if cmd == "stop"
        # stop calculating
        # best move has to be returned
    end
    if cmd == "ponderhit"
        # continue calculating, now as a normal search
    end
    if cmd == "quit"
        # stop engine alltogether
    end
end
